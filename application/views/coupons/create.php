<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
       
        <div class="page-heading">
            <h1>Create coupon</h1>

            <div class="options">

            </div>
        </div>
        <div class="container-fluid">

            <?php $this->load->view('includes/notification'); ?>
            <div class="col-sm-6" style="float:none;margin:auto;">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>Create Coupon</h2>
                    </div>
                    <?php echo form_open('coupon/create', ['id' => "validate-form", "class" => "form-horizontal"]); ?>

                    <div class="panel-body">
                        <div class="form-group mb-md">
                            
                            <div class="col-xs-12">
                                <div class="col-xs-12">
                                <label>Coupon Code</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="code" id="coupon_code"
                                       placeholder="" required>
                                </div>
                                <div class="col-sm-4">
                                    <button type="button" id="generate_coupon" name="generate_coupon" class="btn btn-warning btn-raised">Generate</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-12">
                                <label>Coupon Usage</label>
                            </div>
                            <div class="col-sm-12">
                                <input type="number" class="form-control" name="total_usage" id="total_usage"
                                       placeholder="Number of times Coupon can be used." required>
                            </div>
                        </div>
                    </div>
                        <div class="form-group mb-md">                            
                            <div class="col-xs-12">
                                <label>Coupon Amount</label>
                                <input type="text" class="form-control" name="coupon_value" id="coupon_value"
                                       placeholder="" required>
                            </div>
                        </div>
                        <div class="form-group mb-md">
                             
                            <div class="col-xs-12">
                                <label>Assign User</label>
                                <select class="form-control" id="select1" name="user" required>
                                    <?php foreach($users as $user): ?>
                                    <option value="<?= $user->id ?>"><?= $user->full_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group mb-md">
                            <div class="col-xs-12">
                                 <label>Assign Product</label>
                                <select class="form-control" id="select2" name="product" required>
                                    <?php foreach($products as $product): ?>
                                    <option value="<?= $product->id ?>"><?= $product->product_name ?> - <?= $product->property_type ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        



                    </div>
                    <div class="panel-footer">
                        <div class="clearfix">
                            <button type="submit" name="create_cupon" class="btn btn-primary btn-raised pull-right">Create Coupon</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
