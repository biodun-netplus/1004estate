<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
        
        <div class="page-heading">
            <h1>Announcement</h1>

            <div class="options">

            </div>
        </div>
        <div class="container-fluid">
        <div class="row"> 
         <?php if ($this->aauth->is_member('Admin')) {?>
        <a href="<?php echo site_url('product/announcement/add'); ?>">
        <button class="btn btn-primary btn-raised pull-left" type="button">Add New  Announcement</button>
        </a>
        <?php } ?>
		<?php if(!empty($this->session->flashdata('flashMsg'))){?>
                <div class="alert alert-success">
                    <button class="close" data-close="alert"></button>
                    <span> <?php echo $this->session->flashdata('flashMsg')?></span>
                </div>
           <?php } ?>
         <?php if(isset($actionType) && $actionType=='add'){?>
            <div data-widget-group="group1">
                <div class="col-md-12">
                                <div id="form-errors" class="row"></div>

                                <div id="customer-info" class="row">
                                
                                <form action="<?php echo base_url().'product/announcement/add';?>" method="post" enctype="multipart/form-data">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Tittle</label>
                                            <input class="form-control" data-error="Please input Tittle" placeholder="Enter Tittle" required="required" type="text" name="tittle">
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Description</label>
                                          <textarea class="form-control" id="exampleFormControlTextarea1" rows="4" name="description"></textarea>
                                            
                                        </div>
                                    </div>
                                    
                                    
                                  
                                    
                                    
                                   <div class="col-sm-12 center">
                                        <div class="form-group">
                                          <input type="submit" name="save" value="Save" class="btn btn-primary btn-raised pull-right">
                                        </div>
                                    </div>
                                   </form> 

                                </div>
                            </div>


                <div class="row">
                    
                </div>

            </div>
        <?php } else if(isset($actionType) && $actionType=='edit'){?>
            <div data-widget-group="group1">
                <div class="col-md-12">
                                <div id="form-errors" class="row"></div>

                                <div id="customer-info" class="row">
                                <form action="<?php echo base_url().'product/announcement/edit/'.$records['id']?>" method="post" enctype="multipart/form-data">
                                    <div class="col-sm-6">
                                    <div class="form-group">
                                            <label>Tittle</label>
                                            <input class="form-control" data-error="Please input Tittle" placeholder="Enter Tittle" required="required" type="text" name="tittle" value="<?php echo $records['tittle']?>">
                                            
                                        </div>
                                        
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Description</label>
                                          <textarea class="form-control" id="exampleFormControlTextarea1" rows="4" name="description"><?php echo $records['description']?></textarea>
                                            
                                        </div>
                                    </div>
                                    
                                    
                                    <input type="hidden" name="id" value="<?php echo $records['id'];?>">
                                    
                                   <div class="col-sm-12 center">
                                        <div class="form-group">
                                          <input type="submit" name="save" value="Save" class="btn btn-primary btn-raised pull-right">
                                        </div>
                                    </div>
                                   </form> 
                                </div>
                            </div>


                <div class="row">
                    
                </div>

            </div>
        <?php }else{ ?>
         <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h2>Announcement list</h2>
                <div class="panel-ctrls"></div>
              </div>
              <div class="panel-body no-padding">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>Title</th>
                    <th>Description</th>
                      <?php if ($this->aauth->is_member('Admin')) {?>
                      <th>Action</th>
                      <?php } ?>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
						$i=1;
						if(isset($records) && count($records)>0){
						foreach($records as $LoopRecord){?>
                    <tr>
                      <td> <?php echo $LoopRecord->tittle ?></td>
                       <td> <?php echo $LoopRecord->description ?></td>
                        <?php if ($this->aauth->is_member('Admin')) {?>
                      
                      <td><a href="<?php echo site_url('product/announcement/edit/'.$LoopRecord->id); ?>">Edit</a>
                       |
                       <a href="<?php echo site_url('product/announcement/delete/'.$LoopRecord->id); ?>">Delete</a>
                       </td>
                        <?php } ?>
                    </tr>
                    <?php $i++;}}?>
                  </tbody>
                </table>
              </div>
              <div class="panel-footer"></div>
            </div>
          </div>
        </div>
        
           <?php } ?>

        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
                