<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <style>
table, td, th {    
    border: 1px solid #ddd;text-align: left;
}

table {
    border-collapse: collapse;width: 100%;
}

th, td {
    padding: 15px;
}
</style>
</head>

<body style="width: 100% !important;min-width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100% !important;margin: 0;padding: 0;">

    <b>Dear <?php echo $customer_name; ?>,</b>

    <p>Thanks for your order!</p>

    <p>Your order has been received by
        <?php echo $company_name; ?>.</p>

    <table style="border: 1px solid #ddd;text-align: left;border-collapse: collapse;width: 100%;">
        <thead>
            <th style="border: 1px solid #ddd;text-align: left;padding: 15px;">Product</th>
            <th style="border: 1px solid #ddd;text-align: left;padding: 15px;">Price</th>
        </thead>
        <?php foreach($order_items as $item): ?>
        <?php foreach($item as $product=>$price): ?>
        <tr>
            <td style="border: 1px solid #ddd;text-align: left;padding: 15px;">
                <?php echo $product; ?>
            </td>
            <td style="border: 1px solid #ddd;text-align: left;padding: 15px;">
                <?php echo 'NGN'.number_format($price); ?>
            </td>
        </tr>
        <?php endforeach; ?>
        <?php endforeach; ?>
    </table>
    <p>Kindly used this link
        <a href=" <?php echo $url; ?>"><?php echo $url; ?></a> to complete your payment .</p>

    <p>If you have any question, kindly contact
        <?php echo $merchant_phone; ?> or NetPlusPay team at info@netpluspay.com</p>
    <p>Thank you.</p>

</body>

</html>