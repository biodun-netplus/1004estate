<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')): ?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>

        <div class="page-heading">
            <h1>Admin</h1>

            <div class="options">
                <form action="<?php echo site_url('users/admin') ?>" method="post">
                    <input type="text" name="date_search" id="date_search">
                    <input type="submit" name="Search" value="Search">
                </form>

            </div>
        </div>
        <div class="container-fluid">
            <div class="row"><a href="<?php echo site_url('users/admin/add'); ?>">
                    <button class="btn btn-primary btn-raised pull-left" type="button">Add New Admin</button>
                </a>

                <?php if (!empty($this->session->flashdata('flashMsg'))) { ?>
                    <div class="alert alert-success">
                        <button class="close" data-close="alert"></button>
                        <span> <?php echo $this->session->flashdata('flashMsg') ?></span>
                    </div>
                <?php } ?>
                <?php if (isset($actionType) && $actionType == 'add') { ?>
                    <div data-widget-group="group1">
                        <div class="col-md-12">
                            <div id="form-errors" class="row"></div>

                            <div id="customer-info" class="row">

                                <form action="<?php echo base_url() . 'users/admin/add'; ?>" method="post"
                                      enctype="multipart/form-data">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Full Name</label>
                                            <input class="form-control" data-error="Please input Name"
                                                   placeholder="Enter Name" required="required" type="text"
                                                   name="full_name">

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Mobile Number</label>
                                            <input class="form-control" data-error="Please input Mobile Nummber"
                                                   placeholder="Enter Mobile Nummber" required="required" type="text"
                                                   name="mobile_no">

                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>User Role</label>
                                            <select name="user_role" id="" required class="form-control">
                                                <option value="Admin">Admin</option>
                                                <option value="Finance">Finance</option>
                                                <option value="Report">Report</option>
                                            </select>

                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" data-error="Please input Email"
                                                   placeholder="Enter Email" required="required" type="text"
                                                   name="email">

                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input class="form-control" data-error="Please input Password"
                                                   placeholder="Enter Password" required="required" type="text"
                                                   name="pass">

                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 center">
                                        <div class="form-group">
                                            <input type="submit" name="save" value="Save"
                                                   class="btn btn-primary btn-raised pull-right">
                                        </div>
                                    </div>
                                </form>

                            </div>


                        </div>


                        <div class="row">

                        </div>

                    </div>
                <?php } else if (isset($actionType) && $actionType == 'edit') { ?>
                    <div data-widget-group="group1">
                        <div class="col-md-12">
                            <div id="form-errors" class="row"></div>

                            <div id="customer-info" class="row">
                                <form action="<?php echo base_url() . 'users/admin/edit/' . $records['id'] ?>"
                                      method="post" enctype="multipart/form-data">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Full Name</label>
                                            <input class="form-control" data-error="Please input Name"
                                                   value="<?php echo $records['full_name'] ?>" placeholder="Enter Name"
                                                   required="required" type="text" name="full_name">

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Mobile Nummber</label>
                                            <input class="form-control" data-error="Please input Mobile Nummber"
                                                   placeholder="Enter Mobile Nummber" required="required" type="text"
                                                   name="mobile_no" value="<?php echo $records['mobile_no'] ?>">

                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <?php $current_user_group = $this->aauth->get_user_groups($records['id'])[0]->name; ?>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>User Role</label>
                                            <select name="user_role" id="" required class="form-control">
                                                <option
                                                    value="Admin" <?php echo ($current_user_group == 'Admin') ? 'selected' : ''; ?>>
                                                    Admin
                                                </option>
                                                <option
                                                    value="Finance" <?php echo ($current_user_group == 'Finance') ? 'selected' : ''; ?>>
                                                    Finance
                                                </option>
                                                <option
                                                    value="Report" <?php echo ($current_user_group == 'Report') ? 'selected' : ''; ?>>
                                                    Report
                                                </option>
                                            </select>

                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" data-error="Please input Email"
                                                   placeholder="Enter Email" required="required" type="text"
                                                   name="email" value="<?php echo $records['email'] ?>">

                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>

                                    <input type="hidden" name="id" value="<?php echo $records['id']; ?>">

                                    <div class="col-sm-12 center">
                                        <div class="form-group">
                                            <input type="submit" name="save" value="Save"
                                                   class="btn btn-primary btn-raised pull-right">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                        <div class="row">

                        </div>

                    </div>
                <?php } else { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>Product list</h2>

                                    <div class="panel-ctrls"></div>
                                </div>
                                <div class="panel-body no-padding">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Role</th>
                                            <th>Email</th>
                                            <th>Mobile Number</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i = 1;
                                        if (isset($records) && count($records) > 0) {
                                            foreach ($records as $LoopRecord) {
                                                if($this->aauth->get_user()->id !== $LoopRecord->id):
                                                ?>
                                                <tr>
                                                    <td>
                                                        <a href="<?php echo base_url() ?>users/profile/<?php echo $LoopRecord->id ?>"> <?php echo $LoopRecord->full_name ?></a>
                                                    </td>
                                                    <?php $current_user_group = $this->aauth->get_user_groups($LoopRecord->id)[0]->name; ?>
                                                    <td> <?php echo $current_user_group ?></td>
                                                    <td> <?php echo $LoopRecord->email ?></td>
                                                    <td> <?php echo $LoopRecord->mobile_no ?></td>
                                                    <td>
                                                        <a href="<?php echo site_url('users/admin/edit/' . $LoopRecord->id); ?>">Edit</a>
                                                        |
                                                        <a href="<?php echo site_url('users/admin/delete/' . $LoopRecord->id); ?>">Delete</a>
                                                    </td>
                                                </tr>
                                                <?php
                                                endif;
                                                $i++;
                                            }
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="panel-footer"></div>
                            </div>
                        </div>
                    </div>

                <?php } ?>

            </div>
            <!-- .container-fluid -->
        </div>
        <!-- #page-content -->
    </div>
                