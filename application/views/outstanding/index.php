
<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No :
                <?= $this->aauth->get_user()->meter_no ?>
  </span>
        <?php endif; ?>
        <div class="page-heading">
            <h1>Outstanding Bills</h1>
            <div class="options"> </div>
        </div>
        <div class="container-fluid">
            <?php $this->load->view('includes/notification'); ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <a href="<?php echo site_url('outstanding/create'); ?>" class="btn btn-info btn-raised">Create New Outstanding Payment</a>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2>Outstanding bill List</h2>
                                <div class="panel-ctrls"></div>
                            </div>

                            <div class="panel-body">
                                <table id="defaultTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>Description</th>
                                        <th>Amount</th>
                                        <th>Order ID</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($outstanding_payments as $outstanding){?>
                                        <tr>
                                            <td> <?php echo $outstanding->full_name ?></td>
                                            <td> <?php echo $outstanding->description ?></td>
                                            <td> N<?php echo number_format($outstanding->amount) ?></td>
                                            <td> <?php echo $outstanding->order_id ?></td>
                                            <td> <?php echo ($outstanding->status == 0) ? 'Pending' : 'Paid' ?></td>
                                            <td>
                                                <?php if($outstanding->status == 0):?>
                                                    <a href="<?php echo site_url('outstanding/delete/'.$outstanding->id); ?>">Delete</a>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>


</body>