<div class="col-sm-6 delivery">
    <div class="form-group">
        <label>Delivery Fee</label>
        <input class="form-control" data-error="Please input customer's delivery fee" placeholder="Enter Delivery Fee" required="required" type="text" type="text" number name="delivery_fee" pattern="\d+(\.\d{1,2})?" data-pattern-error="Price must be numeric">
        <div class="help-block form-text with-errors form-control-feedback"></div>
    </div>
</div>
<div class="col-sm-6 delivery">
    <div class="form-group">
        <label>Delivery Street</label>
        <input class="form-control" data-error="Please input customer's delivery street" placeholder="Enter Delivery Street" required="required" type="text" name="delivery_street">
        <div class="help-block form-text with-errors form-control-feedback"></div>
    </div>
</div>
<div class="col-sm-6 delivery">
    <div class="form-group">
        <label>Delivery City</label>
        <input class="form-control" data-error="Please input customer's delivery city" placeholder="Enter Delivery City" required="required" type="text" name="delivery_city">
        <div class="help-block form-text with-errors form-control-feedback"></div>
    </div>
</div>

<div class="col-sm-6 delivery">
    <div class="form-group">
        <label>Delivery State</label>
        <select  data-error="Please select delivery state" name="delivery_state" class="form-control" required="required">
            <option></option>
            <option>ABUJA FCT</option>
            <option>ABIA</option>
            <option>ADAMAWA</option>
            <option>AKWA IBOM</option>
            <option>ANAMBRA</option>
            <option>BAUCHI</option>
            <option>BAYELSA</option>
            <option>BENUE</option>
            <option>BORNO</option>
            <option>CROSS RIVER</option>
            <option>DELTA</option>
            <option>EBONYI</option>
            <option>EDO</option>
            <option>EKITI</option>
            <option>ENUGU</option>
            <option>GOMBE</option>
            <option>IMO</option>
            <option>JIGAWA</option>
            <option>KADUNA</option>
            <option>KANO</option>
            <option>KATSINA</option>
            <option>KEBBI</option>
            <option>KOGI</option>
            <option>KWARA</option>
            <option>LAGOS</option>
            <option>NASSARAWA</option>
            <option>NIGER</option>
            <option>OGUN</option>
            <option>ONDO</option>
            <option>OSUN</option>
            <option>OYO</option>
            <option>PLATEAU</option>
            <option>RIVERS</option>
            <option>SOKOTO</option>
            <option>TARABA</option>
            <option>YOBE</option>
            <option>ZAMFARA</option>
        </select>
        <div class="help-block form-text with-errors form-control-feedback"></div>
    </div>
</div>