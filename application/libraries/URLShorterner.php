<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
class URLShorterner 
{
	var $key;
	var $url;
	var $ci;
	var $client;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->key = $this->ci->config->item('API_KEY');
		$this->url = $this->ci->config->item('API_URL');
		$this->client = new Client(['headers' => [ 'Content-Type' => 'application/json']]);
	}
	public function shorten($link)
	{
		try{
			$request = $this->client->post($this->url, ['query' => ['key' => $this->key],'json' =>['longUrl' => $link]]);
			return json_decode($request->getBody(), true);
		}catch(ClientException $e)
		{
			$response = $e->getResponse();
    		$responseBodyAsString = $response->getBody()->getContents();
    		return $responseBodyAsString;
		}
		
		
	}
	
}