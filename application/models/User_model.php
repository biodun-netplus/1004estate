<?php

class User_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
//we will use the select function  


    /* Function get dynamic data when send all info ($condition,$tableName)*/
    public function get_conditionData($data, $tableName)
    {

        $this->db->select('*');
        $this->db->from($tableName);
        $this->db->where($data);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $FinalData = $query->row_array();

    }

    public function user_type_record($userType = NULL, $date = NULL)
    {

        $this->db->select('aauth_users.*');
        $this->db->from('aauth_users');
        $this->db->join('aauth_user_to_group', 'aauth_user_to_group.user_id=aauth_users.id', 'inner');
        $this->db->join('aauth_groups', 'aauth_groups.id=aauth_user_to_group.group_id', 'inner');
        $this->db->where('aauth_groups.name', $userType);
        if ($date != NULL) {
            $this->db->like('aauth_users.date_created', $date);
        }
        $query = $this->db->get();
        return $data = $query->result();

    }

    public function user_type_array_record($userType = array(), $date = NULL)
    {

        $this->db->select('aauth_users.*');
        $this->db->from('aauth_users');
        $this->db->join('aauth_user_to_group', 'aauth_user_to_group.user_id=aauth_users.id', 'inner');
        $this->db->join('aauth_groups', 'aauth_groups.id=aauth_user_to_group.group_id', 'inner');
        $this->db->where_in('aauth_groups.name', $userType);
        if ($date != NULL) {
            $this->db->like('aauth_users.date_created', $date);
        }
        $query = $this->db->get();
        return $data = $query->result();

    }

    public function all_user($dateFrom = NULL, $dateTo = NULL)
    {

        $this->db->select('aauth_users.*,aauth_groups.name');
        $this->db->from('aauth_users');
        $this->db->join('aauth_user_to_group', 'aauth_user_to_group.user_id=aauth_users.id', 'inner');
        $this->db->join('aauth_groups', 'aauth_groups.id=aauth_user_to_group.group_id', 'inner');
        if ($dateFrom != NULL) {
            $this->db->where('aauth_users.date_created >=', $dateFrom);
            $this->db->where('aauth_users.date_created <=', $dateTo);
        }

        $query = $this->db->get();
        return $data = $query->result();

    }

    public function regular_user()
    {
        $this->db->select('aauth_users.*, aauth_user_to_group.*');
        $this->db->from('aauth_users');
        $this->db->join('aauth_user_to_group', 'aauth_user_to_group.user_id = aauth_users.id', 'inner');
        $this->db->where('aauth_user_to_group.group_id =', '2');
        
        $query = $this->db->get();
        return $data = $query->result();
    }


    public function getConditionalRecords($tab, $condi, $orderBy = NULL, $lim = NULL)
    {

        $this->db->select('*');
        $this->db->from($tab);
        $this->db->where($condi);
        $query = $this->db->get();
        $resultSet = $query->result();

        return $resultSet;
    }


    //Get Conditational Record Group By

    public function getConditionalRecordsGroupBy($tab, $condi, $groupBy = NULL, $order = NULL)
    {
        $this->db->select('*');
        $this->db->from($tab);
        $this->db->where($condi);
        if (!empty($groupBy)) {
            $this->db->group_by($groupBy);
        }
        $query = $this->db->get();
        $resultSet = $query->result();

        return $resultSet;
    }


    public function updateConditionalRecords($tblName, $data, $conditionArray)
    {
        $this->db->where($conditionArray);
        $res = $this->db->update($tblName, $data);
        return $res;
    }

    public function delete($table, $condition)
    {
        $this->db->where($condition);
        $delete = $this->db->delete($table);
        return $delete;
    }

    public function getRecord($table)
    {
        $this->db->select('*');
        $this->db->from($table);
        $query = $this->db->get();
        return $query->result();
    }


    public function setRecord($table, $data)
    {
        $insert = $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function updateRecord($table, $data, $condition)// update ordering of listing
    {
     //   die;
        $this->db->where('id', $condition);
        $update = $this->db->update($table, $data);
        return $update;
    }

    public function updateStyleImageList($table, $data, $conditionArr)
    {
        $this->db->where($condition);
        $update = $this->db->update($table, $data);
        return $update;
    }

    public function trucateTable($table)
    {
        $res = $this->db->truncate($table);
        return $res;
    }

    public function updateServiceCharge($table, $data, $condition){
        $this->db->where('id', $condition);
       $update =  $this->db->update($table, array('meter_no' => $data));
       return $update;
    }

    public function getUserById($condition){
        $this->db->select('*');
        $this->db->from('aauth_users');
        $this->db->where('id', $condition);
        $query = $this->db->get();
        $resultSet = $query->result();

        return $resultSet;
    }

}

?>