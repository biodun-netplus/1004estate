-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 14, 2019 at 03:12 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `buenavisate_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `aauth_groups`
--

DROP TABLE IF EXISTS `aauth_groups`;
CREATE TABLE IF NOT EXISTS `aauth_groups` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `definition` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_groups`
--

INSERT INTO `aauth_groups` (`id`, `name`, `definition`) VALUES
(1, 'Admin', 'Administrator'),
(2, 'Public', 'Public Access Group'),
(4, 'Merchant', 'Merchant Accounts'),
(5, 'Finance', 'Finance User Group'),
(6, 'Report', 'Report User Group');

-- --------------------------------------------------------

--
-- Table structure for table `aauth_group_to_group`
--

DROP TABLE IF EXISTS `aauth_group_to_group`;
CREATE TABLE IF NOT EXISTS `aauth_group_to_group` (
  `group_id` int(11) UNSIGNED NOT NULL,
  `subgroup_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`group_id`,`subgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_login_attempts`
--

DROP TABLE IF EXISTS `aauth_login_attempts`;
CREATE TABLE IF NOT EXISTS `aauth_login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(39) DEFAULT '0',
  `timestamp` datetime DEFAULT NULL,
  `login_attempts` tinyint(2) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aauth_login_attempts`
--

INSERT INTO `aauth_login_attempts` (`id`, `ip_address`, `timestamp`, `login_attempts`) VALUES
(11, '127.0.0.1', '2018-12-24 17:46:00', 2),
(14, '127.0.0.1', '2018-12-29 12:43:06', 3),
(15, '127.0.0.1', '2018-12-29 12:59:40', 3),
(20, '127.0.0.1', '2018-12-29 21:23:12', 1),
(27, '127.0.0.1', '2019-01-09 14:59:13', 5);

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perms`
--

DROP TABLE IF EXISTS `aauth_perms`;
CREATE TABLE IF NOT EXISTS `aauth_perms` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `definition` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perm_to_group`
--

DROP TABLE IF EXISTS `aauth_perm_to_group`;
CREATE TABLE IF NOT EXISTS `aauth_perm_to_group` (
  `perm_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`perm_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perm_to_user`
--

DROP TABLE IF EXISTS `aauth_perm_to_user`;
CREATE TABLE IF NOT EXISTS `aauth_perm_to_user` (
  `perm_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`perm_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_pms`
--

DROP TABLE IF EXISTS `aauth_pms`;
CREATE TABLE IF NOT EXISTS `aauth_pms` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) UNSIGNED NOT NULL,
  `receiver_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  `date_read` datetime DEFAULT NULL,
  `pm_deleted_sender` int(1) DEFAULT NULL,
  `pm_deleted_receiver` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `full_index` (`id`,`sender_id`,`receiver_id`,`date_read`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_users`
--

DROP TABLE IF EXISTS `aauth_users`;
CREATE TABLE IF NOT EXISTS `aauth_users` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `pass` varchar(200) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `cug_no` varchar(255) DEFAULT NULL,
  `meter_no` varchar(255) DEFAULT NULL,
  `house_address` varchar(255) NOT NULL,
  `type_of_property` varchar(255) NOT NULL,
  `type_of_ownership` varchar(50) DEFAULT NULL,
  `partner_type` varchar(255) NOT NULL,
  `banned` tinyint(1) DEFAULT 0,
  `last_login` datetime DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `login_count` int(11) DEFAULT 0,
  `date_created` datetime DEFAULT NULL,
  `forgot_exp` text DEFAULT NULL,
  `remember_time` datetime DEFAULT NULL,
  `remember_exp` text DEFAULT NULL,
  `verification_code` text DEFAULT NULL,
  `totp_secret` varchar(16) DEFAULT NULL,
  `ip_address` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_users`
--

INSERT INTO `aauth_users` (`id`, `email`, `pass`, `username`, `full_name`, `mobile_no`, `cug_no`, `meter_no`, `house_address`, `type_of_property`, `type_of_ownership`, `partner_type`, `banned`, `last_login`, `last_activity`, `login_count`, `date_created`, `forgot_exp`, `remember_time`, `remember_exp`, `verification_code`, `totp_secret`, `ip_address`) VALUES
(1, 'manieabiodun@gmail.com', '0d7f995ee8c6b3921fce84215655d4144ad7581753f4efc0f1318764fa3ce78b', '', 'Manasseh Abiodun', '08142810918', '', '8099994345', '11 Ogundoju Street', 'Semi Detached', 'landlord', 'Partner', 0, '2019-06-28 11:49:28', '2019-06-28 11:49:28', 18, '2019-03-21 20:10:28', NULL, NULL, NULL, NULL, NULL, '127.0.0.1'),
(3, 'manassehl9@gmail.com', 'f02af0215a09369087b091fe6aefedd5d077732cedb1cbc053f798b8aa61b604', '', 'Manie Resident', '08142810918', '', '62317012086', '11 Ogundoju Street', 'Terrace', 'landlord', 'Partner', 0, '2019-06-28 11:59:01', '2019-06-28 11:59:01', 7, '2019-03-21 21:38:14', NULL, NULL, NULL, NULL, NULL, '127.0.0.1'),
(4, 'olamide@gmail.com', 'a1dc90875895cb65399a5a3e394393e3aaa8b5541e2d29c5f490cc2902ae39dd', '', 'Olamide', '07080891355', NULL, NULL, '', '', NULL, 'Partner', 0, '2019-03-22 16:16:06', '2019-03-22 16:16:06', 3, '2019-03-21 21:51:09', NULL, NULL, NULL, NULL, NULL, '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `aauth_user_to_group`
--

DROP TABLE IF EXISTS `aauth_user_to_group`;
CREATE TABLE IF NOT EXISTS `aauth_user_to_group` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_user_to_group`
--

INSERT INTO `aauth_user_to_group` (`user_id`, `group_id`) VALUES
(1, 1),
(3, 2),
(4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `aauth_user_variables`
--

DROP TABLE IF EXISTS `aauth_user_variables`;
CREATE TABLE IF NOT EXISTS `aauth_user_variables` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `data_key` varchar(100) NOT NULL,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

DROP TABLE IF EXISTS `announcement`;
CREATE TABLE IF NOT EXISTS `announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tittle` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `cart_session` varchar(255) NOT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `meter_no` varchar(255) NOT NULL,
  `product_price_type` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `product_type` varchar(255) NOT NULL DEFAULT 'PRODUCT',
  `date` date NOT NULL,
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `coupon_value` double NOT NULL,
  `total_usage` int(11) NOT NULL,
  `usage_count` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `user_id`, `product_id`, `order_id`, `coupon_value`, `total_usage`, `usage_count`, `status`, `created_at`) VALUES
(3, 'CODE1548864658796', '25', '6', NULL, 10000, 10, 0, 0, '2019-01-30 17:11:22'),
(5, 'CODE1550652663857', '28', '1', NULL, 30000, 5, 4, 0, '2019-02-20 09:51:20');

-- --------------------------------------------------------

--
-- Table structure for table `house_address`
--

DROP TABLE IF EXISTS `house_address`;
CREATE TABLE IF NOT EXISTS `house_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `house_number` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=197 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `house_address`
--

INSERT INTO `house_address` (`id`, `house_number`) VALUES
(1, 'A4'),
(2, 'A5'),
(3, 'A6'),
(4, 'A7'),
(5, 'A8'),
(6, 'A9'),
(7, 'A10'),
(8, 'A11'),
(9, 'A12'),
(10, 'A13'),
(11, 'A19'),
(12, 'A20'),
(13, 'A21'),
(14, 'A22'),
(15, 'A23'),
(16, 'A24'),
(17, 'B5'),
(18, 'B6'),
(19, 'B7'),
(20, 'B8'),
(21, 'B9'),
(22, 'B10'),
(23, 'B11'),
(24, 'B12'),
(25, 'B13'),
(26, 'B14'),
(27, 'B15'),
(28, 'B16'),
(29, 'B17'),
(30, 'B18'),
(31, 'B19'),
(32, 'B20'),
(33, 'B21'),
(34, 'B22'),
(35, 'B23'),
(36, 'B1A'),
(37, 'B1B'),
(38, 'B1C'),
(39, 'B2A'),
(40, 'B2B'),
(41, 'B2C'),
(42, 'C5'),
(43, 'C6'),
(44, 'C7'),
(45, 'C8'),
(46, 'C9'),
(47, 'C10'),
(48, 'C11'),
(49, 'C12'),
(50, 'C13'),
(51, 'C14'),
(52, 'C15'),
(53, 'C16'),
(54, 'C17'),
(55, 'C18'),
(56, 'C19'),
(57, 'C20'),
(58, 'D5'),
(59, 'D6'),
(60, 'D7'),
(61, 'D8'),
(62, 'D9'),
(63, 'D10'),
(64, 'D11'),
(65, 'D12'),
(66, 'D13'),
(67, 'D14'),
(68, 'D15'),
(69, 'D16'),
(70, 'D17'),
(71, 'D18'),
(72, 'D19'),
(73, 'D20'),
(74, 'E5'),
(75, 'E8'),
(76, 'E9'),
(77, 'F7'),
(78, 'F8'),
(79, 'F9'),
(80, 'F10'),
(81, 'F11'),
(82, 'F12'),
(83, 'F13'),
(84, 'F14'),
(85, 'F15'),
(86, 'F16'),
(87, 'G5'),
(88, 'G6'),
(89, 'G7'),
(90, 'G8'),
(91, 'G9'),
(92, 'H1'),
(93, 'H2'),
(94, 'H3'),
(95, 'H4'),
(96, 'H5'),
(97, 'H6'),
(98, 'H7'),
(99, 'H8'),
(100, 'H9'),
(101, 'H10'),
(102, 'H11'),
(103, 'H12'),
(104, 'H13'),
(105, 'H14'),
(106, 'H15'),
(107, 'H16'),
(108, 'H17'),
(109, 'H18'),
(110, 'H19'),
(111, 'H20'),
(112, 'H21'),
(113, 'H22'),
(114, 'H23'),
(115, 'H24'),
(116, 'H25'),
(117, 'H26'),
(118, 'H27'),
(119, 'H28'),
(120, 'H29'),
(121, 'I1A'),
(122, 'I1B'),
(123, 'I1C'),
(124, 'I1D'),
(125, 'I2A'),
(126, 'I2B'),
(127, 'I2C'),
(128, 'I2D'),
(129, 'I4A'),
(130, 'I4B'),
(131, 'I4C'),
(132, 'I4D'),
(133, 'I5A'),
(134, 'I5B'),
(135, 'I5C'),
(136, 'I5D'),
(137, 'I6A'),
(138, 'I6B'),
(139, 'I6C'),
(140, 'I6D'),
(141, 'I7A'),
(142, 'I7B'),
(143, 'I8A'),
(144, 'I8B'),
(145, 'I8C'),
(146, 'I8D'),
(147, 'I9A'),
(148, 'I9B'),
(149, 'I9C'),
(150, 'I9D'),
(151, 'I10A'),
(152, 'I10B'),
(153, 'I10C'),
(154, 'I10D'),
(155, 'I11A'),
(156, 'I11B'),
(157, 'I11C'),
(158, 'I11D'),
(159, 'I11E'),
(160, 'I11F'),
(161, 'I12A'),
(162, 'I12B'),
(163, 'I12C'),
(164, 'I12D'),
(165, 'I12E'),
(166, 'I12F'),
(167, 'I13A'),
(168, 'I13B'),
(169, 'I13C'),
(170, 'I13D'),
(171, 'I13E'),
(172, 'I13F'),
(173, 'I15A'),
(174, 'I15B'),
(175, 'I15C'),
(176, 'I15D'),
(177, 'I16A'),
(178, 'I16B'),
(179, 'I16C'),
(180, 'I16D'),
(181, 'I17A'),
(182, 'I17B'),
(183, 'I17C'),
(184, 'I17D'),
(185, 'I19A'),
(186, 'I19B'),
(187, 'I19C'),
(188, 'I19D'),
(189, 'I20A'),
(190, 'I20B'),
(191, 'I20C'),
(192, 'I20D'),
(193, 'I21A'),
(194, 'I21B'),
(195, 'I21C'),
(196, 'I21D');

-- --------------------------------------------------------

--
-- Table structure for table `meter_tariff`
--

DROP TABLE IF EXISTS `meter_tariff`;
CREATE TABLE IF NOT EXISTS `meter_tariff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tariff_name` varchar(255) NOT NULL,
  `tariff_price` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `outstanding_bills`
--

DROP TABLE IF EXISTS `outstanding_bills`;
CREATE TABLE IF NOT EXISTS `outstanding_bills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `meter_no` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `amount` double NOT NULL,
  `status` int(5) NOT NULL DEFAULT 0,
  `order_id` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_id` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 1,
  `transaction_id` varchar(100) DEFAULT NULL,
  `admin_payment` int(11) NOT NULL DEFAULT 0,
  `status` varchar(50) NOT NULL,
  `bank` varchar(100) DEFAULT NULL,
  `amount_paid` double DEFAULT NULL,
  `narration` text DEFAULT NULL,
  `payment_description` text DEFAULT NULL,
  `meter_no` varchar(100) DEFAULT NULL,
  `token_no` varchar(255) DEFAULT NULL,
  `token_desc` varchar(255) DEFAULT NULL,
  `token_amount` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `payment_type` varchar(50) NOT NULL DEFAULT 'Card',
  `coupon_id` int(11) DEFAULT NULL,
  `vend_log` longtext DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `coupon_id` (`coupon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `payment_id`, `user_id`, `amount`, `quantity`, `transaction_id`, `admin_payment`, `status`, `bank`, `amount_paid`, `narration`, `payment_description`, `meter_no`, `token_no`, `token_desc`, `token_amount`, `type`, `payment_type`, `coupon_id`, `vend_log`, `date_created`) VALUES
(1, '55083297416', 3, 6100, 1, '55083297416', 1, 'Paid', '', 6100, NULL, 'Successful', '8099994345', '4859 9945 9546', '095869495', '10', 'Power', 'Cash', NULL, '{\"token_no\":\"4859 9945 9546\",\"token_desc\":\"095869495\",\"token_amount\":\"10\"}', '2019-05-22 15:31:37'),
(2, '55630792148', 3, 400, 1, '55630792148', 1, 'Paid', '', 400, NULL, 'Successful', '8099994345', NULL, NULL, NULL, 'Power', 'Cash', NULL, 'false', '2019-06-15 22:27:00'),
(3, '55029316487', 3, 400, 1, '55029316487', 1, 'Paid', '', 400, NULL, 'Successful', '8099994345', NULL, NULL, NULL, 'Power', 'Cash', NULL, 'false', '2019-06-15 22:28:54'),
(4, '55937106428', 3, 400, 1, '55937106428', 1, 'Paid', '', 400, NULL, 'Successful', '8099994345', NULL, NULL, NULL, 'Power', 'Cash', NULL, 'false', '2019-06-15 22:30:05'),
(5, '55481679320', 3, 400, 1, '55481679320', 1, 'Paid', '', 400, NULL, 'Successful', '8099994345', NULL, NULL, NULL, 'Power', 'Cash', NULL, 'false', '2019-06-15 22:30:32'),
(6, '55173240068', 3, 400, 1, '55173240068', 1, 'Paid', '', 400, NULL, 'Successful', '8099994345', '1050  7787  7727  8185  3032', 'Success', NULL, 'Power', 'Cash', NULL, '{\"token_no\":\"1050  7787  7727  8185  3032\",\"token_desc\":\"Success\",\"token_amount\":null}', '2019-06-15 22:32:32'),
(7, '55496213870', 3, 2000, 1, '55496213870', 1, 'Paid', '', 2000, NULL, 'Successful', '62317012086', NULL, NULL, NULL, 'Power', 'Cash', NULL, NULL, '2019-06-16 17:25:07'),
(8, '55489721630', 3, 2000, 1, '55489721630', 1, 'Paid', '', 2000, NULL, 'Successful', '62317012086', NULL, NULL, NULL, 'Power', 'Cash', NULL, NULL, '2019-06-16 17:26:18'),
(9, '55347169082', 3, 2000, 1, '55347169082', 1, 'Paid', '', 2000, NULL, 'Successful', '62317012086', NULL, NULL, NULL, 'Power', 'Cash', NULL, 'false', '2019-06-16 17:28:13'),
(10, '55347169082', 3, 2000, 1, NULL, 1, 'Pending', NULL, NULL, NULL, NULL, '62317012086', NULL, NULL, NULL, 'Power', 'Cash', NULL, NULL, '2019-06-16 17:28:14'),
(11, '55413678209', 3, 2000, 1, '55413678209', 1, 'Paid', '', 2000, NULL, 'Successful', '62317012086', NULL, NULL, NULL, 'Power', 'Cash', NULL, 'false', '2019-06-16 17:29:59');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) NOT NULL,
  `product_price` varchar(255) NOT NULL,
  `take_or_pay` varchar(255) NOT NULL,
  `base_charge` varchar(255) NOT NULL,
  `payment_type_id` int(11) NOT NULL,
  `account_number` text NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `property_type` varchar(255) NOT NULL,
  `product_type` varchar(255) NOT NULL,
  `partner_type` varchar(255) NOT NULL,
  `product_status` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_name`, `product_price`, `take_or_pay`, `base_charge`, `payment_type_id`, `account_number`, `bank_name`, `product_image`, `property_type`, `product_type`, `partner_type`, `product_status`, `created`) VALUES
(1, 'Service Charge', '20000', '', '20000', 0, '0060799442', 'Zenith', '', 'Terrace', 'Service', 'Partner', 0, '2019-03-21 20:41:41'),
(3, 'Light bill', '0', '', '0', 0, '0060799442', 'Zenith', '', 'Terrace', 'Power', 'Partner', 0, '2019-03-22 07:52:37');

-- --------------------------------------------------------

--
-- Table structure for table `service_charges`
--

DROP TABLE IF EXISTS `service_charges`;
CREATE TABLE IF NOT EXISTS `service_charges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meter_no` varchar(250) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `amount_due` int(11) DEFAULT NULL,
  `total_paid` int(11) DEFAULT 0,
  `advance_payment` int(11) NOT NULL DEFAULT 0,
  `advance_month` int(11) NOT NULL DEFAULT 0,
  `date_created` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `meter_no` (`meter_no`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_id`, `name`, `password`, `email`, `mobile_no`) VALUES
(1, 15, 'first second', 'da0fdc4ba72117e4495e1a9df336b957a5de7f641d5abf2c428b0f1d1e55ee8a', 'manie94@gmail.com', 99847384);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
